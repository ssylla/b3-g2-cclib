package fr.epsi.cclib;

import fr.epsi.cclib.exception.CSetEmptyException;
import fr.epsi.cclib.exception.CSetIndexOutOfBoundException;
import fr.epsi.cclib.exception.CSetGenericException;

import java.util.Arrays;

public class CSetArray implements ICSet {
	
	public static final int DEFAULT_MAX_SIZE = 10;
	
	private Object[] data;
	private int currentIndex;
	
	{
		currentIndex = -1;
	}
	
	public CSetArray() {
		data = new Object[DEFAULT_MAX_SIZE];
	}
	
	public CSetArray( int maxSize ) {
		data = new Object[maxSize];
	}
	
	public CSetArray( Object[] data ) {
		this.data = data;
	}
	
	@Override
	public void add( Object object ) throws CSetGenericException {
		if ( !contains( object ) ) {
			data[++currentIndex] = object;
		} else {
			throw new CSetGenericException( 15, "Existe déjà" );
		}
	}
	
	@Override
	public boolean isEmpty() {
		return currentIndex <= -1;
	}
	
	@Override
	public Object get( int index ) throws CSetIndexOutOfBoundException {
		if ( index < 0 || index > currentIndex ) {
			throw new CSetIndexOutOfBoundException( index, getSize() );
		}
		return data[index];
	}
	
	@Override
	public boolean contains( Object object ) {
		boolean found = false;
		for ( Object item : data ) {
			if ( item == object ) {
				found = true;
				break;
			}
		}
		return found;
	}
	
	@Override
	public int getSize() {
		return currentIndex + 1;
	}
	
	@Override
	public int getMaxSize() {
		return data.length;
	}
	
	@Override
	public ICSet copy() {
		return new CSetArray( Arrays.copyOf( data, data.length ) );
	}
	
	@Override
	public Object remove( int index ) throws CSetEmptyException, CSetIndexOutOfBoundException {
		if ( currentIndex <= -1 ) {
			throw new CSetEmptyException( "Attention le tableau est vide..." );
		}
		if ( index < 0 || index > currentIndex ) {
			throw new CSetIndexOutOfBoundException( index, getSize() );
		}
		Object result = null;
		if ( index <= currentIndex ) {
			result = data[index];
			for ( int i = index; i < currentIndex; ++i ) {
				data[i] = data[i + 1];
			}
			currentIndex--;
		}
		return result;
	}
	
	@Override
	public void merge( ICSet set ) {
		for ( int i = 0; i < set.getSize(); ++i ) {
			try {add( set.get( i ) );} catch ( CSetIndexOutOfBoundException e ) {
				e.printStackTrace();
			} catch ( CSetGenericException CSetGenericException ) {
				CSetGenericException.printStackTrace();
			}
		}
	}
}
