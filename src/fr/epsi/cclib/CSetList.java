package fr.epsi.cclib;

import fr.epsi.cclib.exception.CSetEmptyException;
import fr.epsi.cclib.exception.CSetIndexOutOfBoundException;
import fr.epsi.cclib.exception.CSetGenericException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CSetList implements ICSet {
	
	List data;
	
	public CSetList() {
		data = new ArrayList();
	}
	
	public CSetList( List data ) {
		this.data = data;
	}
	
	@Override
	public void add( Object object ) throws CSetGenericException {
		if ( !contains( object )) {
			data.add( object );
		}
	}
	
	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}
	
	@Override
	public Object get( int index ) throws CSetIndexOutOfBoundException {
		if ( index < 0 || index > data.size() ) {
			throw new CSetIndexOutOfBoundException( index, getSize() );
		}
		return data.get( index );
	}
	
	@Override
	public boolean contains( Object object ) {
		return data.contains(object);
	}
	
	@Override
	public int getSize() {
		return data.size();
	}
	
	@Override
	public int getMaxSize() {
		return Integer.MAX_VALUE;
	}
	
	@Override
	public ICSet copy() {
		List newData = new ArrayList();
		Collections.copy( newData, data );
		return new CSetList( newData );
		
	}
	
	@Override
	public Object remove( int index ) throws CSetEmptyException, CSetIndexOutOfBoundException {
		return data.remove( index );
	}
	
	@Override
	public void merge( ICSet set ) {
		for(int i = 0; i<set.getSize(); ++i) {
			try {add( set.get( i ) );} catch ( CSetIndexOutOfBoundException e ) {
				e.printStackTrace();
			} catch ( CSetGenericException CSetGenericException ) {
				CSetGenericException.printStackTrace();
			}
		}
	}
}
