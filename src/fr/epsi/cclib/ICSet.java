package fr.epsi.cclib;

import fr.epsi.cclib.exception.CSetEmptyException;
import fr.epsi.cclib.exception.CSetIndexOutOfBoundException;
import fr.epsi.cclib.exception.CSetGenericException;

public interface ICSet {
	
	void add( Object object ) throws CSetGenericException;
	
	boolean isEmpty();
	
	Object get( int index ) throws CSetIndexOutOfBoundException;
	
	boolean contains( Object object );
	
	int getSize();
	
	int getMaxSize();
	
	ICSet copy();
	
	Object remove( int index ) throws CSetEmptyException, CSetIndexOutOfBoundException;
	
	void merge( ICSet set );
}
