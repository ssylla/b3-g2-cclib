package fr.epsi.cclib.exception;

public class CSetEmptyException extends Exception {
	
	public CSetEmptyException( String message ) {
		super( message );
	}
}
