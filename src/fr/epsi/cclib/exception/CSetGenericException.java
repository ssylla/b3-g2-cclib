package fr.epsi.cclib.exception;

public class CSetGenericException extends Exception {
	private int code;
	private String message;
	
	public CSetGenericException( int code, String message ) {
		this.code = code;
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return code + " - " + message;
	}
	
	public int getCode() {
		return code;
	}
}
